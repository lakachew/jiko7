<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//temporary welcome view route
Route::get('/', "Controller@index");


Route::auth();

//Route::get('/home', 'HomeController@index');

/*
Route::group(['prefix' => 'api/v1', 'middleware' => 'auth:api'], function()
{
    Route::post('worklogs', 'WorklogsController@store');

});*/



Route::group(['prefix' => 'api/v3' ], function()
{
    //Route::resource('authenticate', 'Auth\AuthenticateController', ['only' => ['index']]);
    Route::post('authenticate', 'Auth\AuthenticateController@authenticate');
    Route::post('authenticate/user', 'Auth\AuthenticateController@getAuthenticatedUser');

});

Route::group(['prefix' => 'api/v3', 'middleware' => 'jwt:auth'], function()
{
    //Route::resource('authenticate', 'Auth\AuthenticateController', ['only' => ['index']]);
    //Route::post('authenticate', 'Auth\AuthenticateController@authenticate');
    //Route::post('authenticate/user', 'Auth\AuthenticateController@getAuthenticatedUser');

});





Route::group(['prefix' => 'api/v1', 'middleware' => 'auth:api'], function()
{
    Route::post('worklogs', 'WorklogsController@store');

});

Route::group(['prefix' => 'api/v1', ], function()
{
    // Mobile Login routes
    //Route::resource('authenticate', 'Auth\AuthenticateController', ['only' => ['index']]);
    Route::post('authenticate', 'Auth\AuthenticateController@authenticate');



    //web accessing
    Route::get('worklogs/currentweek', 'WorklogsController@currentWeek');
    Route::get('worklogs/home', 'WorklogsController@index');
    //OR
    //Route::resource('worklogs', 'WorklogsController');

});


