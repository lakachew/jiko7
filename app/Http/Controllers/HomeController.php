<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Worklog;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $worklogsMysql = Worklog::all();

        //dd($worklogsMysql);

        return view('home')->with('worklogs', $worklogsMysql);
    }
}
