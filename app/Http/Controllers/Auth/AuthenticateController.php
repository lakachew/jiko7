<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\User;
use Illuminate\Http\Request;
use Acme\Transformers\WorklogTransformer as WorklogTransformer;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class authenticateController extends ApiController
{
    //

    /**
     * Acme\Transformers\WorklogTransformer
     *
     * @var WorklogTransformer
     */
    protected $worklogTransformer;

    /**
     * WorklogsController constructor.
     *
     * @param Acme\ //Transformers\WorklogTransformer $worklogTransformer
     */

    public function __construct(WorklogTransformer $worklogTransformer)
    {
        $this->worklogTransformer = $worklogTransformer;
    }


    /**
     * Authenticate the user when request for authentication using JWT
     *
     */
    
    protected function authenticate()
    {

        //check if all required arguments have values
        if(!Input::get('email') or !Input::get('password'))
        {
            return $this->respondMissing('missing credential');
        }

        $email = Input::get('email');
        $password = Input::get('password');

        try{
            if (! $token = JWTAuth::attempt(['email' => $email, 'password' => $password])) {
                //return response()->json(['error' => 'invalid_credentials'], 401);
                return $this->invalidCredential('invalid cridentials');
            }
        }catch (JWTException $e) {
            // something went wrong
            //return response()->json(['error' => 'could_not_create_token'], 500);
            return $this->respondNotFound('could_not_create_token');
        }

        //$userMysql = DB::table('users')->where('id', $user->id)->get();

        //get the authenticated user and convert it to an array
        $user = Auth::user();
        $userArray = $user->toArray();

        $token2 = JWTAuth::fromUser($user, $userArray);

        //return response()->json(compact('token'));
        return $this->jsonResponse($token2);

    }


    /***
     * Returns user cridential for an Authenticated access using JWT
     * @return json
     */
    protected function getAuthenticatedUser()
    {

        try {

            if (! $token = JWTAuth::parseToken()->authenticate()) {
                return $this->invalidCredential('user_not_found');
            }

        } catch (TokenExpiredException $e) {
            return $this->exceptionThrowResponse("token_expired", $e->getStatusCode());

        } catch (TokenInvalidException $e) {
            return $this->exceptionThrowResponse("token_invalid", $e->getStatusCode());

        } catch (JWTException $e) {
            return $this->exceptionThrowResponse("token_absent", $e->getStatusCode());

        }

        // the token is valid and we have found the user via the sub claim
        return $this->jsonResponse($token);

    }


    /**
     * Authenticate the user when request for authentication
     * @return json
     */
    /*
    protected function authenticate(Request $userFromMobile)
    {

        //check if all required arguments have values
        if(!Input::get('email') or !Input::get('password'))
        {
            return $this->respondMissing('missing credential');
        }

        $userServer = User::where('email', '=', Input::get('email'))->take(1)->get();

        if($userServer->count() > 0)
        {

            //dd($userServer[0]->password);
            //dd($userFromMobile->password);

            if($userServer[0]->password = $userFromMobile->password)
            {
                Auth::onceUsingId($userServer[0]->id);

                if (Auth::check())
                {
                    // The user is logged in...
                    //dd($user[0]->email);
                    return $this->jsonResponse($userServer[0]);
                    //return Response::json($userServer->latest($userServer[0]->id)->get());
                }
            }

        }

        return $this->respondNotFound('Wrong user cridentials');
        //return Response::json($userServer->all());

    }*/
}
