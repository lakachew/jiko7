<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Worklog as Worklog;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Acme\Transformers\WorklogTransformer as WorklogTransformer;

class WorklogsController extends ApiController
{


    public $restful = true;

    /**
     * Acme\Transformers\WorklogTransformer
     *
     * @var WorklogTransformer
     */
    protected $worklogTransformer;

    /**
     * WorklogsController constructor.
     *
     * @param Acme\ //Transformers\WorklogTransformer $worklogTransformer
     */

    public function __construct(WorklogTransformer $worklogTransformer)
    {
        $this->middleware('auth');
        $this->worklogTransformer = $worklogTransformer;
    }


    /**
     * collect the Worklogs data and returns it to the view.
     *
     * @return Worklog
     */
    public function index()
    {
        //collecting all the resources from the database
        $worklogsObj = Worklog::all();

        //dd($worklogsObj);
        //return the collected data as an Array
        return view('worklog.index')->with('worklogs', $worklogsObj);

        /*$worklogsJsonIncoded = $this->respond([
            'data' => $this->worklogTransformer->transformCollection($worklogsMysql->all())]
        );*/
        // or $worklogsJsonDecoded = response()->json($worklogsMysql);

    }

    /**
     * collect Worklog data for a sertain date range
     * 
     * @return Worklog
     */
    public function currentWeek()
    {
        //$currentDateAndTime = date_create(now());
        //dd($currentDateAndTime);

        //$worklogsMysql = DB::table('worklogs')->whereBetween('created_at', ['2013-04-00 00:00:00','2015-04-00 00:00:00'] )->get();

        //dd($worklogsMysql);
        $worklogsModel = Worklog::where('created_at', '>', '2013-04-00 00:00:00')
            ->where('created_at', '<','2016-04-00 00:00:00' )->get();

        //return the collected data as a Oject of Worklog
        return view('worklog.currentweek')->with('worklogs', $worklogsModel);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //collecting the worklog info by the provided ID return a fail if not found
        //$worklog = Worklog::findOrFail($id);

        $worklog = Worklog::find($id);

        //$worklogs = Worklog::latest('id')->get();

        if(! $worklog)
        {
            return $this->respondNotFound('worklog does not exist');
        }

        return $this->respond([
            'data' => $this->worklogTransformer->transform($worklog)
        ]);

    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //just for testing the store function
        dd('store worklog');


        /*'user_id', 'building_name', 'house_no', 'address_from_user', 'description',
        'start_longitude', 'start_latitude', 'end_longitude', 'end_latitude'*/
        if(! Input::get('user_id') or ! Input::get('start_longitude') or ! Input::get('start_latitude'))
        {
            return $this->respondMissing('Missing required parameters.');
        }

        Worklog::create(Input::all());

        return $this->respondCreated('Worklog was successfully created.');


        /*
        $input = Request::all(); //feches all input request

        if(is_null($input))
            return false;

        Worklog::create($input);*/


        //$response["success"] = 1;
        $statusCode = 200;
        //return Response::setContent ($response);


        //return Response::json($response, $statusCode);
        //return response()->json(['name' => 'Abigail', 'state' => 'CA']);

        //return true;

        //Temporary Testing : 1
        //$worklogs = Worklog::all();

        /*return Response::json([
            'data' => $this->transformCollection($worklogs->all())
        ], 200);*/
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


}
