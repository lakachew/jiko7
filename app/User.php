<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone', 'privilege', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'updated_at', 'remember_token'
    ];

    /**
     * function for making our Work model use a bridge table for connecting with the user table
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function works()
    {
        return $this->belongsToMany('App\Work');
        //or explicitlly define as follows if we didn't define the Models in conventional way
        //belongsToMany(ModelToRelateTo, BridgeTableName, ColumnNameForCurrentModel, ColumnNameForJoiningModel)
        //return $this->belongsToMany('App\Work', 'user_work', 'user_id', 'work_id');
    }
}
