<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worklog extends Model
{


    protected $fillable = [
        'user_work_id', 'start_time', 'end_time', 'description',
        'start_longitude', 'start_latitude', 'end_longitude', 'end_latitude'
    ];

    public function user_works()
    {
        return $this->belongsTo('App\UserWork');
    }


    public function getEmployeeName(){

        return "change the Worklog model method";

        return User::where('id', $this->user_id)->first()->first_name;
    }
    
}
