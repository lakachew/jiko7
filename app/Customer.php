<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login_name', 'company_name', 'contact_name', 'email', 'phone', 'address', 'created_at', 'updated_at',
    ];
}
