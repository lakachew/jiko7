<?php
/**
 * Created by PhpStorm.
 * User: lakachew
 * Date: 23/04/2016
 * Time: 18:55
 */

namespace Acme\Transformers;


class WorklogTransformer extends Transformer
{

    /**
     * will Transforms a single object /worklog/
     *
     *
     * @param $worklog
     * @return array
     */
    public function transform($worklog)
    {

        return [
            'id' => $worklog['id'],
            'user_id' => $worklog['user_id'],
            'building_name' => $worklog['building_name'],
            'house_no' => $worklog['house_no'],
            'address_from_user' => $worklog['address_from_user'],
            'description' => $worklog['description'],
            'start_longitude' => $worklog['start_longitude'],
            'start_latitude' => $worklog['start_latitude'],
            'created at' => $worklog['created_at'],
            'updated at' => $worklog['updated_at'],
            'end_longitude' => $worklog['end_longitude'],
            'end_latitude' => $worklog['end_latitude'],
        ];
    }

}