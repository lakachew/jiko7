<?php
/**
 * Created by PhpStorm.
 * User: lakachew
 * Date: 23/04/2016
 * Time: 18:53
 */

namespace Acme\Transformers;


use App\Http\Requests;


abstract class Transformer
{

    /**
     * Will map over a collection
     *
     *
     * @param $items
     * @return array
     */
    public function transformCollection(array $items)
    {
        return array_map([$this, 'transform'], $items);
    }

    /***
     * abstract function since every transformCollection needs to be transform each item
     * 
     * @param $item
     * @return mixed
     */
    public abstract function transform($item);

}