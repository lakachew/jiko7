<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
    protected $fillable = [
        'id', 'customer_id', 'finished', 'address', 'description', 'created_at', 'updated_at'
    ];

    public function customers()
    {
        return $this->belongsTo('App\Customer');
    }

    /**
     * Function for making Many to many relation
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
        //or explicitlly define as follows: (ModelToRelateTo, BridgeTableName, ColumnNameForCurrentModel, ColumnNameForJoiningModel)
        //return $this->belongsToMany('App\User', 'user_work', 'work_id', 'user_id');
    }

}
