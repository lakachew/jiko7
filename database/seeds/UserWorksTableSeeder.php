<?php

use Illuminate\Database\Seeder;
use \App\UserWork as UserWork;
use Faker\Factory as Faker;

class UserWorksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        foreach (range(1,30) as $index)
        {
            UserWork::create([
                'user_id' => $faker->numberBetween(1,10),
                'work_id' => $faker->numberBetween(1,30),
            ]);
        }
    }
}
