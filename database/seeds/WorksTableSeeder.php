<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Work as Work;

class WorksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create();

        foreach (range(1,30) as $index)
        {
            Work::create([
                'customer_id' => $faker->numberBetween(1,25),
                'finished' => $faker->boolean,
                'address' => $faker->streetAddress,
                'description' => $faker->sentence,
                'created_at' => $faker->dateTimeThisYear,
                'updated_at' => $faker->dateTimeThisYear
            ]);
        }
    }

    //'updated_at' => $faker->dateTimeBetween('+1 days', '+2 years')->format('m/d/Y'),
}
