<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorklogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('worklogs', function(Blueprint $table)
        {
            $table->increments('id');

            $table->integer('user_works_id')->unsigned();
            $table->foreign('user_works_id')->references('id')->on('user_works');
            
            $table->text('description')->nullable();
            $table->double('start_longitude');
            $table->double('start_latitude');
            $table->double('end_longitude')->nullable();
            $table->double('end_latitude')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('worklogs');
    }
}
