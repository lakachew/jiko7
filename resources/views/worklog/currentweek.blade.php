<!-- /**
 * Created by PhpStorm.
 * User: lakachew
 * Date: 30/05/2016
 * Time: 18:06
 */-->

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Current Week</div>

                    <div align="center" class="panel-body">
                        <button type="button" class="btn btn-info btn-arrow-left"><</button>
                        <button type="button" class="btn btn-link">Week: number </button>
                        <button type="button" class="btn btn-info btn-arrow-right">></button>
                    </div>

                    <div class="panel-body">
                        <h2>Weekly Worklogs</h2>
                        <p>Weekly logs are shown:</p>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>User ID</th>
                                <th>First Name</th>
                                <th>Building Name</th>
                                <th>Longitude</th>
                                <th>Latitude</th>
                                <th>Created at</th>
                                <th>Updated at</th>
                            </tr>
                            </thead>
                            <tbody>

                            <@foreach($worklogs as $worklog)
                                    <tr>
                                        <td>{{ $worklog->user_id }}</td>
                                        <td>{{ $worklog->getEmployeeName() }}</td>
                                        <td>{{ $worklog->building_name }}</td>
                                        <td>{{ $worklog->start_longitude }}</td>
                                        <td>{{ $worklog->start_latitude }}</td>
                                        <td>{{ $worklog->created_at }}</td>
                                        <td>{{ $worklog->updated_at }}</td>
                                    </tr>
                            @endforeach


                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">
                        15 worklog's recorded
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
