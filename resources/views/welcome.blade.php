@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome</div>

                <div class="panel-body">
                    Welcome to Jiko Oy Web application.
                    <p>this site is only for registered users so please login if you are a registered member.</p>
                    <p>check out the contact us link on top of this page to get intouch with us.</p>
                    <p>Thank you for visiting us!!! </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
