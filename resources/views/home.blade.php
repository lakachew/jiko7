@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div align="center" class="panel-body">
                    <button type="button" class="btn btn-info btn-arrow-left"><</button>
                    <button type="button" class="btn btn-link">Week: </button>
                    <button type="button" class="btn btn-info btn-arrow-right">></button>
                </div>

                <div class="panel-body">
                    <h2>All Worklogs</h2>
                    <p>Weekly logs are shown:</p>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>User ID</th>
                            <th>First Name</th>
                            <th>Building Name</th>
                            <th>Longitude</th>
                            <th>Latitude</th>
                            <th>Created at</th>
                            <th>Updated at</th>
                        </tr>
                        </thead>
                        <tbody>


                        <@foreach($worklogs as $worklog)
                            <tr>
                                <td>{{ $worklog->user_id }}</td>
                                <td>{{ $worklog->getEmployeeName() }}</td>
                                <td>{{ $worklog->building_name }}</td>
                                <td>{{ $worklog->start_longitude }}</td>
                                <td>{{ $worklog->start_latitude }}</td>
                                <td>{{ $worklog->created_at }}</td>
                                <td>{{ $worklog->updated_at }}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="panel-body">
                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
